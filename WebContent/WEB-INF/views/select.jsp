<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.mylab2.util.URLConstants"%>
<%@ page session="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>REPLY TOPIC PAGE</title>
</head>
<body>
	<div
		style="text-align: right; background-color: indigo; color: white; height: 50px">
		<a href='<c:url value='${ URLConstants.URL_LOGOUT }'></c:url>'
			class="btn btn-danger" role="button" aria-pressed="true"
			style="margin-top: 6px; margin-right: 20px; float: right;">Đăng
			xuất</a>
		<p
			style="font-family: sans-serif; margin-top: 15px; margin-right: 20px; float: right;">
			<c:out value='${sessionScope.USER_LOGIN}' />
		</p>
	</div>
	<div class="container">
		<div class="row" style="margin-top: 30px; margin-bottom: 20px">
			<form class="form" style="width: 70%;">
				<div class="row">
					<label class="my-1 mr-2" for="inlineFormCustomSelectPref">Chọn
						quận:</label> <select class="custom-select my-1 mr-sm-2" id="idQuan">
						<c:forEach items='${ listQuan }' var="item" varStatus="loop">
							<option selected="selected" disabled="disabled" hidden>Bấm để chọn quận</option>
							<option value="${ item.idQuan }">${ item.tenQuan }</option>
						</c:forEach>
					</select>
				</div>
				<div class="row" style="margin-top: 30px;">
					<label class="my-1 mr-2" for="inlineFormCustomSelectPref">Chọn
						phường:</label> <select class="custom-select my-1 mr-sm-2" id="idPhuong">
					</select>
				</div>
				<div style="float: right; margin-top: 20px;">
					<button type="submit" class="btn btn-primary my-1">Submit</button>
				</div>
			</form>
		</div>

	</div>
</body>
<!-- CSS only -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
	integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk"
	crossorigin="anonymous">

<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
	integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
	crossorigin="anonymous"></script>
<script
	src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
	integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
	crossorigin="anonymous"></script>
<script
	src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"
	integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI"
	crossorigin="anonymous"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery3_5_1.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('#idQuan').change(function() {
			var idQuan = $(this).val();
			$.ajax({
				type : 'GET',
				data : "idQuan=" + idQuan,
				url : 'selectajax',
				success : function(result) {
					console.log(result);
					$('#idPhuong').html(result);

				}

			});
		});
	});
</script>

</html>