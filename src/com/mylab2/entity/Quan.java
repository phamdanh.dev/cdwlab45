package com.mylab2.entity;

public class Quan {
	
	private int idQuan;
	private String tenQuan;
	
	public Quan() {
		// TODO Auto-generated constructor stub
	}

	public Quan(int idQuan, String tenQuan) {
		super();
		this.idQuan = idQuan;
		this.tenQuan = tenQuan;
	}

	public int getIdQuan() {
		return idQuan;
	}

	public void setIdQuan(int idQuan) {
		this.idQuan = idQuan;
	}

	public String getTenQuan() {
		return tenQuan;
	}

	public void setTenQuan(String tenQuan) {
		this.tenQuan = tenQuan;
	}
	
	

}
