package com.mylab2.entity;

public class Phuong {
	
	private int idPhuong;
	private String tenPhuong;
	private int idQuan;
	
	public Phuong() {
		// TODO Auto-generated constructor stub
	}

	public Phuong(int idPhuong, String tenPhuong, int idQuan) {
		super();
		this.idPhuong = idPhuong;
		this.tenPhuong = tenPhuong;
		this.idQuan = idQuan;
	}

	public int getIdPhuong() {
		return idPhuong;
	}

	public void setIdPhuong(int idPhuong) {
		this.idPhuong = idPhuong;
	}

	public String getTenPhuong() {
		return tenPhuong;
	}

	public void setTenPhuong(String tenPhuong) {
		this.tenPhuong = tenPhuong;
	}

	public int getIdQuan() {
		return idQuan;
	}

	public void setIdQuan(int idQuan) {
		this.idQuan = idQuan;
	}
	
	

}
