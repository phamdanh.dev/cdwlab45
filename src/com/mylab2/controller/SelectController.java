package com.mylab2.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mylab2.dao.PhuongDAO;
import com.mylab2.dao.QuanDAO;
import com.mylab2.entity.Phuong;
import com.mylab2.entity.Quan;
import com.mylab2.util.PathConstants;
import com.mylab2.util.URLConstants;

@WebServlet(urlPatterns = { URLConstants.URL_SELECT, URLConstants.URL_SELECT_AJAX })
public class SelectController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private PhuongDAO phuongDAO = null;
	private QuanDAO quanDAO = null;

	@Override
	public void init() throws ServletException {
		phuongDAO = new PhuongDAO();
		quanDAO = new QuanDAO();
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");
		resp.setCharacterEncoding("UTF-8");
		String action = req.getServletPath();
		System.out.println(action);
		switch (action) {
		case URLConstants.URL_SELECT:
			List<Quan> listQuan = quanDAO.getAllQuan();
			req.setAttribute("listQuan", listQuan);
			req.getRequestDispatcher(PathConstants.PATH_SELECT).forward(req, resp);
			break;

		case URLConstants.URL_SELECT_AJAX:
			System.out.println("DA VAO TRONG AJAX");
			int idQuan = Integer.parseInt(req.getParameter("idQuan"));
			List<Phuong> listPhuong = new ArrayList<Phuong>();
			listPhuong = phuongDAO.getListPhuongByIdQuan(idQuan);
			for (Phuong phuong : listPhuong) {
				resp.getWriter().println(
						"<option value ='" + phuong.getIdPhuong() + "'>" + phuong.getTenPhuong() + "</option>");
			}
			
			break;

		}

	}

}
