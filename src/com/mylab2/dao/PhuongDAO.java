package com.mylab2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mylab2.connection.JDBCConnection;
import com.mylab2.entity.Phuong;

public class PhuongDAO {

	public List<Phuong> getListPhuongByIdQuan(int idQuan) {
		String query = "select * from phuong where phuong.idQuan = ?";
		List<Phuong> listPhuong = new ArrayList<Phuong>();
		try (Connection connection = JDBCConnection.getConnection()) {
			PreparedStatement preparedStatement = connection.prepareStatement(query);
			preparedStatement.setString(1, String.valueOf(idQuan));
			ResultSet rs = preparedStatement.executeQuery();
			while (rs.next()) {
				Phuong phuong = new Phuong();
				phuong.setIdQuan(rs.getInt("idPhuong"));
				phuong.setIdQuan(rs.getInt("idQuan"));
				phuong.setTenPhuong(rs.getString("tenPhuong"));
				listPhuong.add(phuong);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listPhuong;
	}

}
