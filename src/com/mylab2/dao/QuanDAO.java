package com.mylab2.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.mylab2.connection.JDBCConnection;
import com.mylab2.entity.Quan;

public class QuanDAO {

	public List<Quan> getAllQuan() {
		List<Quan> listQuan = new ArrayList<Quan>();

		String query = "select * from quan";

		try (Connection connection = JDBCConnection.getConnection()) {
			PreparedStatement statement = connection.prepareStatement(query);
			ResultSet rs = statement.executeQuery();
			while (rs.next()) {
				Quan quan = new Quan();
				quan.setIdQuan(rs.getInt("idQuan"));
				quan.setTenQuan(rs.getString("tenQuan"));
				listQuan.add(quan);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listQuan;
	}

}
