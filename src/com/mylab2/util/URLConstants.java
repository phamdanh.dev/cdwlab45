package com.mylab2.util;

public class URLConstants {
	
	public static final String URL_LOGIN = "/login";
	public static final String URL_SELECT = "/select";
	public static final String URL_LOGOUT = "/logout";
	public static final String URL_SELECT_AJAX = "/selectajax";

}
